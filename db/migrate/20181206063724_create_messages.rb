class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.references :message_group
      t.string :name, default: '774'
      t.text :message
      t.timestamps
    end
  end
end
