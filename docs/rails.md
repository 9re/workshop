---

# Rails勉強会 2018.12.6
## Taro Kobayashi


---

# 今日やること

1. formatの話
1. バリデーション、スコープ(ActiveRecord)
1. 例外処理の定石(クレカ登録してない奴はxxで登録ページへ飛ばしたいとか)
1. kaminari, gonの紹介
1. fixture
1. rspec紹介(とツラミの話controller_spec request_spec違いすぎてgooglability悪い話)
1. オブジェクト指向プログラミング

---

# 1. formatの話
## formatの話とajax

---

# formatとは、

```console
rails routes
```

の

```
product GET    /products/:id(.:format)
```

のフォーマットの部分

---

# アクションの挙動

デフォルトの挙動は、formatの部分のみを変えた、同じURLは同じコントローラーの同じアクションに処理が渡る。

アクション側の設定は以下の通り、

```ruby
def show
  @product = Product.find(params[:product_id])
  respond_to do |format|
    format.json {}
    format.html { @page_title_title = 'タイトル' }
  end
end
```

---

# viewの挙動

`<action>` . `format` . `processor` の命名規則で所定のディレクトリに配置

```
app/views/products
├── show.html.haml
└── show.js.erb
```

action側でテンプレート名をしている場合も同様

```
render 'partials/template_name'
```

```
app/views/partials
├── template_name.html.haml
└── template_name.js.erb
```

---

# ajax

Railsのajaxについては、[jquery-ujs](https://github.com/rails/jquery-ujs)を用いる。

* 非同期処理でRailsで良く使うパターンがまとめてある。
* CSRFトークンのセットアップ等
* jqueryの onclick, $.ajaxなどの処理がほとんどのケースで要らなくなる

---

# 自前でやる場合

HTML

```html
<a href="/some_action" class="some-action">some action</a>
```

JavaScript

```js
$('a.some-action').live('click', function(event) {
  event.preventDefault();
  var self = $(this);
  var url = self.attr('href');
  var csrf_token = $('meta[name=csrf-token]').attr('content');
  $.ajax({url: url, type: 'POST', data: {authenticity_token: csrf_token}});
});
```

---

# jquery-ujs

HTML

```html
<a href="/some_action" class="some-action" data-remote="true" data-method="post">some action</a>
```

JavaScript
```js
// jquery-ujs does
```

---

# Restだった部分をajax化する

一般的な `create` の実装

```ruby
def create
  @post = Post.create(params[:post].require.permit(:name, :content)
  redirect_to show_post_path(@post)
end
```

リダイレクト。このリダイレクトを無くすには?!

やりたいこと:

* @postの内容のhtmlを画面内の所定の位置に追加

---

# 更新の実装

リストに追加する方法だと何通りかある。

* 自分が投稿したものを1個だけリストのhtmlに追加
* 他の人が投稿しているかもしれないので、リスト毎読み直して、リスト自体を動的に書き直し

