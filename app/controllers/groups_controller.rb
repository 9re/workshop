class GroupsController < ApplicationController
  def index
    @groups = MessageGroup.all.order(:updated_at)
  end

  def create
    @group = MessageGroup.new(params.require(:message_group).permit(:name))
    if @group.save
      redirect_to group_path(@group)
    else
      render :new
    end
  end

  def show
    @group = MessageGroup.find(params[:id])
  end
end
